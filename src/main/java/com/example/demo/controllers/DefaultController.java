package com.example.demo.controllers;

import java.security.Principal;
import com.example.demo.models.User;
import com.example.demo.security.CustomUserDetailsService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DefaultController {

    private final CustomUserDetailsService userDetailsService;

    public DefaultController(CustomUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @GetMapping("/")
    public String name(Model model) {
        Principal principal = SecurityContextHolder.getContext().getAuthentication();
        User user = userDetailsService.loadUserByUsername(principal.getName()).getUser();
        model.addAttribute("user", user);
        return "index";
    }
}
