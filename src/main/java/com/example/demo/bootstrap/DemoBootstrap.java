package com.example.demo.bootstrap;

import javax.transaction.Transactional;
import com.example.demo.models.Role;
import com.example.demo.models.User;
import com.example.demo.repositories.RoleRepository;
import com.example.demo.repositories.UserRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DemoBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public DemoBootstrap(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
        log.debug("Creating Roles");
        Role roleAdmin = new Role();
        roleAdmin.setName("admin");
        roleRepository.save(roleAdmin);

        Role roleStaff = new Role();
        roleStaff.setName("staff");
        roleRepository.save(roleStaff);

        Role roleUser = new Role();
        roleUser.setName("user");
        roleRepository.save(roleUser);

        log.debug("Creating User");
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);

        User admin = new User();
        admin.setUsername("admin");
        admin.setPassword(encoder.encode("admin"));
        admin.getRoles().add(roleAdmin);
        admin.getRoles().add(roleStaff);
        userRepository.save(admin);

        User staff = new User();
        staff.setUsername("staff");
        staff.setPassword(encoder.encode("staff"));
        staff.getRoles().add(roleStaff);
        userRepository.save(staff);

        User user = new User();
        user.setUsername("user");
        user.setPassword(encoder.encode("user"));
        user.getRoles().add(roleUser);
        userRepository.save(user);
    }
}
